import React from 'react';
import PropType from 'prop-types';
import { compose } from 'recompose';
import { Tooltip, IconButton as MuiIconButton } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { grey } from '@material-ui/core/colors';
import withWidth from '@material-ui/core/withWidth';
import ErrorOutlineOutlinedIcon from '@material-ui/icons/ErrorOutlineOutlined';

const CustomIconButton = withStyles((theme) => ({
  root: {
    color: grey[500],
    padding: 5,
    borderRadius: 0,
    width: 40,
  },
}))(MuiIconButton);

function IconButton({ children, title, handleClick }) {
  return (
    <Tooltip title={title} arrow>
      <CustomIconButton onClick={handleClick}>{children}</CustomIconButton>
    </Tooltip>
  );
}

IconButton.propTypes = {
  children: PropType.element,
  handleClick: PropType.func,
  title: PropType.string,
};

IconButton.defaultProps = {
  children: <ErrorOutlineOutlinedIcon fontSize="small" edge="start" />,
  handleClick: () => console.log('No implemented function.'),
  title: 'No Title',
};

export default compose(withWidth())(IconButton);
