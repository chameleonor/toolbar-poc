import React, { useState } from 'react';
import PropType from 'prop-types';
import { Tooltip, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';

const styles = () => ({
  autoComplete: {
    width: 160,
    height: '100%',
  },
});

const CustomTextField = withStyles({
  root: {
    height: '100%',

    '& .MuiOutlinedInput-root': {
      height: '100%',
      fontSize: 14,

      '& fieldset': {
        border: 0,
      },
      '&:hover fieldset': {
        border: 0,
      },
      '&.Mui-focused fieldset': {
        border: 0,
      },
      '& .MuiIconButton-root': {
        borderRadius: 0,
        fontSize: 14,
      },
    },
  },
})(TextField);

const SelectBroker = ({ options, placeholder = 'Text Here', id, classes }) => {
  const [value, setValue] = useState();
  const [inputValue, setInputValue] = useState('');

  const [toolTipOpen, setToolTipOpen] = React.useState(false);

  const handleTooltipClose = () => setToolTipOpen(false);
  const handleTooltipOpen = () => setToolTipOpen(true);

  return (
    <Autocomplete
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      inputValue={inputValue}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      id={id}
      options={options}
      size="small"
      renderInput={(params) => (
        <Tooltip
          title={placeholder}
          placement="bottom"
          open={toolTipOpen}
          arrow
        >
          <CustomTextField
            {...params}
            placeholder={placeholder}
            variant="outlined"
            onMouseEnter={handleTooltipOpen}
            onMouseLeave={handleTooltipClose}
            onClick={handleTooltipClose}
          />
        </Tooltip>
      )}
      className={classes.autoComplete}
    />
  );
};

SelectBroker.propTypes = {
  options: PropType.arrayOf(PropType.string),
  placeholder: PropType.string,
  classes: PropType.oneOfType(PropType.object),
  id: PropType.string,
};

// TODO: add defaultprops

export default withStyles(styles)(SelectBroker);
