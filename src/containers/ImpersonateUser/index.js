import React, { useState } from 'react';
import { Menu } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import IconButton from '../../components/IconButton';

const options = ['guiherme.zkxq46f', 'fernando.nbkx12s'];
const title = 'Impersonate User';

function ControllableStates() {
  const [value, setValue] = useState();
  const [inputValue, setInputValue] = useState('');

  return (
    <Autocomplete
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      inputValue={inputValue}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      id="broker"
      options={options}
      style={{ width: '100%' }}
      size="small"
      renderInput={(params) => (
        <TextField {...params} label={title} variant="outlined" />
      )}
    />
  );
}

const StyledMenu = withStyles((theme) => ({
  paper: {
    border: '1px solid #d3d4d5',
    padding: '5px 10px',
    width: 250,
  },
}))((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{ vertical: 'top', horizontal: 'right' }}
    {...props}
  />
));

const ImpersonateUser = () => {
  const [openMenu, setOpenMenu] = useState(null);

  const handleOpen = (event) => {
    console.log('open select book => ', event.currentTarget);
    setOpenMenu(event.currentTarget);
  };

  const handleClose = () => {
    console.log('close');
    setOpenMenu(null);
  };

  return (
    <>
      <IconButton handleClick={handleOpen} title={title}>
        <PeopleAltOutlinedIcon fontSize="small" edge="start" />
      </IconButton>

      <StyledMenu
        id="customized-menu"
        anchorEl={openMenu}
        open={Boolean(openMenu)}
        onClose={handleClose}
        keepMounted
      >
        <ControllableStates />
      </StyledMenu>
    </>
  );
};

export default ImpersonateUser;
