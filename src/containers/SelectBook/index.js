import React from 'react';
import ToolbarSelect from '../../components/ToolbarSelect';

const SelectBook = () => {
  const options = ['XYZ', '123'];

  return (
    <ToolbarSelect options={options} placeholder="Select Book" id="book" />
  );
};

export default SelectBook;
