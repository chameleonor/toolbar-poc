import React from 'react';
import ToolbarSelect from '../../components/ToolbarSelect';

const SelectBroker = () => {
  const options = ['ITAU', 'BBSE'];

  return (
    <ToolbarSelect options={options} placeholder="Select Broker" id="broker" />
  );
};

export default SelectBroker;
