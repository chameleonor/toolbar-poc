import React from 'react';

import {
  AppBar,
  Toolbar as MuiToolBar,
  IconButton as MuiIconButton,
  Box,
} from '@material-ui/core';
import { grey } from '@material-ui/core/colors';

import { withStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';

// icons

import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
import RemoveCircleOutlineOutlinedIcon from '@material-ui/icons/RemoveCircleOutlineOutlined';
import IconButton from '../../components/IconButton';
import SelectBook from '../SelectBook';
import SelectBroker from '../SelectBroker';
import ImpersonateUser from '../ImpersonateUser';

const handleInsertRow = () => console.log('insert row');
const handleRemoveRow = () => console.log('remove row');

const styles = (theme) => ({
  toolbar: {
    background: '#fff',
    minHeight: 40,
    maxHeight: 40,
    height: 40,
    padding: 0,
  },
  iconButton: {
    color: grey[500],
    padding: 5,
    borderRadius: 0,
  },
  boxContent: {
    borderColor: grey[400],
    height: '100%',
  },
});

const Toolbar = (props) => {
  const { classes } = props;

  return (
    <AppBar position="static" elevation={1}>
      <MuiToolBar className={classes.toolbar}>
        <Box display="flex" className={classes.boxContent}>
          <MuiIconButton className={classes.iconButton}>
            <MenuIcon />
          </MuiIconButton>
        </Box>

        <Box display="flex" className={classes.boxContent} borderLeft={1}>
          <IconButton handleClick={handleInsertRow} title="Insert Row">
            <AddCircleOutlineOutlinedIcon fontSize="small" edge="start" />
          </IconButton>

          <IconButton handleClick={handleRemoveRow} title="Remove Row">
            <RemoveCircleOutlineOutlinedIcon fontSize="small" edge="start" />
          </IconButton>
        </Box>

        <Box display="flex" className={classes.boxContent} borderLeft={1}>
          <SelectBook />
        </Box>

        <Box display="flex" className={classes.boxContent} borderLeft={1}>
          <SelectBroker />
        </Box>

        <Box
          display="flex"
          flex={2}
          className={classes.boxContent}
          borderLeft={1}
        />

        <Box display="flex" className={classes.boxContent} borderLeft={1}>
          <ImpersonateUser />
        </Box>
      </MuiToolBar>
    </AppBar>
  );
};

export default withStyles(styles, { withTheme: true })(Toolbar);
