import React from 'react';
import { Grid, Box } from '@material-ui/core';
import TradingViewWidget from 'react-tradingview-widget';

import Toolbar from '../../containers/Toolbar';

const Home = () => {
  return (
    <Box style={{ height: '100%' }} display="flex" flexDirection="column">
      <Box display="flex">
        <Toolbar />
      </Box>
      <Box display="flex" flex={1}>
        <Grid container>
          <Grid item xs={4}>
            <TradingViewWidget symbol="NASDAQ:AAPL" autosize />
          </Grid>
          <Grid item xs={4}>
            2
          </Grid>
          <Grid item xs={4}>
            3
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default Home;
