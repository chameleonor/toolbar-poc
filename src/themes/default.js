import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

const fontFamily = {
  fontFamily: 'Roboto',
  fontStyle: 'normal',
};
const fullHeightAndWidth = {
  height: '100vh',
  width: '100vw',
};
const palette = {
  primary: {
    main: '#dcdcdc',
  },
};

const theme = createMuiTheme({
  typography: {
    ...fontFamily,
  },
  palette: {
    ...palette,
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        body: {
          WebkitFontSmoothing: 'antialiased',
          ...fullHeightAndWidth,
        },
        '#root': {
          ...fullHeightAndWidth,
          background: `${palette.primary} !important`,
        },
        input: {
          ...fontFamily,
        },
        button: {
          ...fontFamily,
        },
        fieldset: {
          ...fontFamily,
          border: 0,
        },
      },
    },
  },
});

export default responsiveFontSizes(theme);
